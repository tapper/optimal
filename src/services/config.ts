import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';


@Injectable()


export class Config
{
    public CustomerId:any = '1';
    public UserId:any = '';
    public PushId:any = '';
    public UserConnected:Object;
    public ServerUrl = 'http://tapper.org.il/baloon/laravel/public/api/'
    public host = 'http://tapper.org.il/baloon/laravel/storage/app/public/'
    public TermsURL = 'http://tapper.org.il/baloon/terms.pdf'
    CourseId:any = '';
    public User:any = '';
    public Substitute:number = 0;
    public JobsCount:number = 0;
    public openJobs:number = 2;
    public openJobsType3:number = 3;
    public openJobsType4:number = 4;
    public TotalSub3:number = 3;
    public TotalSub4:number = 4;
    public Messages:number = 0;
    public Regions: any[] = [];
    public Types: any[] = [];
    public CustumerServerData:any = [];

    constructor() { };


    async SetCustumerServerData(data)
    {
        this.CustumerServerData = data;
    }



    SetUserPush(push_id:any)
    {
        this.PushId = push_id;
        window.localStorage.push_id = push_id;
    }

    getTypeName(id)
    {
        let name = '';

        if(id == 1)
            name = "גננת"
        if(id == 3)
            name = "סייעת"
        if(id == 3)
            name = "גננת מחליפה"
        if(id == 4)
            name = "סייעת מחליפה"

        return name;
    }
};


