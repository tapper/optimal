import { Component,NgZone } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {HomePage} from "../home/home";
import {loginService} from "../../services/loginService";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {Config} from "../../services/config";
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import {ProfilePage} from "../profile/profile";
/**
 * Generated class for the MyAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-account',
  templateUrl: 'my-account.html',
})
export class MyAccountPage {
    
    public recommendArray;
    public sum = 0;

    public CustomerId;
    public host;
    CustumerServerData:any = [];
    public imageLogo:any;
    public myaccountvideo:any;

  constructor(public navCtrl: NavController, public navParams: NavParams , public Login:loginService,public sendtoserver:sent_to_server_service,public zone: NgZone , public Settings:Config,private domSanitizer: DomSanitizer,private alertCtrl: AlertController) {

      this.CustomerId = this.Settings.CustomerId;
      this.host = this.Settings.host;
      this.sendtoserver._JsonInfo.subscribe(val => {
          this.zone.run(() => {
              this.CustumerServerData = val;
              console.log("CustumerServerData",this.CustumerServerData)
              if (this.CustumerServerData) {
                  if (this.CustumerServerData.custumer_data) {
                      if (this.CustumerServerData.custumer_data.length > 0) {
                          this.imageLogo = this.host + this.CustumerServerData.custumer_data[0].image;
                          this.myaccountvideo = this.CustumerServerData.custumer_data[0].my_account_video+'?rel=0';;
                          this.myaccountvideo = this.domSanitizer.bypassSecurityTrustResourceUrl(this.myaccountvideo);
                      }
                  }
              }
          });
      });


      this.Login.getRecommendById('getRecommendById').then((data: any) => {
          console.log("getRecommendById : " , data);
          this.recommendArray = data;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyAccountPage');
  }


    requestWithdraw()
    {

        this.Login.requestWithdraw('requestWithdraw').then(
            (data: any) => {

                let alertTitle = ''
                console.log ("status: ",  data.status);
                if (data.status == 1) {
                    alertTitle = 'בקשתך למשיכה נשלחה בהצלחה'
                }
                 if (data.status == 0) {
                    alertTitle = 'בקשתך התקבלה ובתהליך בדיקה'
                }

                 if (data.status == 2) {
                    alertTitle = 'יש תחילה למלא את כל שדות פרטי בנק בפרטים אישיים'
                }

                console.log("requestWithdraw : ",data);
                let alert = this.alertCtrl.create({
                    title: alertTitle,
                    //subTitle: 'מעקב יופיע בעמודך האישי',
                    cssClass: 'alertCss',
                    buttons: ['סגור']
                });
                alert.present();
                if (data.status != 2)
                    this.navCtrl.setRoot(HomePage);
                else
                    this.navCtrl.push(ProfilePage);

            });
    }


  calculateSum()
  {
      console.log(typeof this.recommendArray)
      this.sum = 0;
       if(this.recommendArray)
       {
           for(let i=0;i<this.recommendArray.length;i++)
               this.sum+=Number(this.recommendArray[i].deal_price);
    
           return this.sum;
       }else return 0;
       
  }

}
