import {Component,NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams,ToastController} from 'ionic-angular';
import {loginService} from "../../services/loginService";
import {Config} from "../../services/config";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import {toastService} from "../../services/toastService";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {ForgotpassPage} from "../forgotpass/forgotpass";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})


export class LoginPage {

    public info: any = {mail: "", password: '' , agreeTerms :false};
    public user:string = '';
    public CustomerId;
    public host;
    CustumerServerData:any = [];
    public imageLogo:any;
    //public agreeTerms:boolean = false;
    public browser;
    public emailregex;



    constructor(public navCtrl: NavController, public navParams: NavParams , public Login:loginService , public Settings:Config, public Toast: toastService, public toastCtrl: ToastController,public sendtoserver:sent_to_server_service,public zone: NgZone, private iab: InAppBrowser) {

        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(val => {
            this.zone.run(() => {
                this.CustumerServerData = val;
                console.log("CustumerServerData",this.CustumerServerData)
                if (this.CustumerServerData) {
                    if (this.CustumerServerData.custumer_data) {
                        if (this.CustumerServerData.custumer_data.length > 0) {
                            this.imageLogo = this.host + this.CustumerServerData.custumer_data[0].image;
                        }
                    }
                }
            });
        });
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad LoginPage');
    }

    toggleTerms(type) {
        this.info.agreeTerms = type;
    }

    openTerms() {
         this.browser = this.iab.create(this.Settings.TermsURL, '_system', 'location=no,toolbar=yes');
    }

    openForgotPassPage() {
        this.navCtrl.push(ForgotpassPage);

    }
    checkLogin() {

        this.emailregex = /\S+@\S+\.\S+/;

        if (this.info['mail'].length == 0)
            this.Toast.presentToast('הכנס כתובת מייל')

        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין')
        }

        else if (this.info['password'].length == 0)
            this.Toast.presentToast('הכנס סיסמה')

        else if (!this.info.agreeTerms) {
            this.Toast.presentToast('יש לאשר תנאי שימוש')
        }
        else {
            //console.log(this.info);
            this.Login.getUserDetails('getUserDetails',this.info).then(
                (data: any) => {
                    if (data[0]) {
                        console.log("UserDetails : ");
                        this.user = data[0];
                        this.setUserSettings(data);
                    }
                    else
                    {
                        console.log("UserDetails : " , data[0]);
                        this.Toast.presentToast('פרטים לא נכונים אנא נסה שנית')
                        this.info['password'] = '';
                    }

                });
        }
    }

    setUserSettings(data)
    {
        console.log("MMM : " , this.user);
        window.localStorage.id = this.user["id"];
        window.localStorage.name = this.user["name"];
        window.localStorage.info = this.user["info"];
        window.localStorage.image = this.user["image"];
        window.localStorage.type = this.user["type_id"];
        this.Settings.UserConnected = this.user;
        this.navCtrl.setRoot(HomePage);
    }

    GoToRegister()
    {
        this.navCtrl.push(RegisterPage);
    }

    public presentToast(text) {
        console.log("f3");
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    }
}
