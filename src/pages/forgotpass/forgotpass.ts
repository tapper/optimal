import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {toastService} from "../../services/toastService";
import {loginService} from "../../services/loginService";
import {Config} from "../../services/config";
import {LoginPage} from "../login/login";

/**
 * Generated class for the ForgotpassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgotpass',
  templateUrl: 'forgotpass.html',
})
export class ForgotpassPage {

    public info: any = {mail: ""};
    public user:string = '';
    public CustomerId;
    public host;
    CustumerServerData:any = [];
    public imageLogo:any;
    //public agreeTerms:boolean = false;
    public browser;
    public emailregex;

  constructor(public navCtrl: NavController, public navParams: NavParams, public Login:loginService , public Settings:Config, public Toast: toastService, public toastCtrl: ToastController,public sendtoserver:sent_to_server_service,public zone: NgZone) {
      this.CustomerId = this.Settings.CustomerId;
      this.host = this.Settings.host;
      this.sendtoserver._JsonInfo.subscribe(val => {
          this.zone.run(() => {
              this.CustumerServerData = val;
              console.log("CustumerServerData",this.CustumerServerData)
              if (this.CustumerServerData) {
                  if (this.CustumerServerData.custumer_data) {
                      if (this.CustumerServerData.custumer_data.length > 0) {
                          this.imageLogo = this.host + this.CustumerServerData.custumer_data[0].image;
                      }
                  }
              }
          });
      });
  }

    sendPassword() {
        this.emailregex = /\S+@\S+\.\S+/;

        if (this.info['mail'].length == 0)
            this.Toast.presentToast('הכנס כתובת מייל')

        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין')
        }
        else {
            this.Login.ForgotPass('ForgotPass',this.info).then(
                (data: any) => {
                    if (data.status == 1) {
                        this.Toast.presentToast('פרטי התחברות נשלחו למייל שהזנת');
                        this.info['mail'] = '';
                        this.navCtrl.setRoot(LoginPage);
                    }
                    else  {
                        this.Toast.presentToast('המייל שהוזן לא קיים במערכת');
                        this.info['mail'] = '';
                    }
                });
        }
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpassPage');
  }

}
