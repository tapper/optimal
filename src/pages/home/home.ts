import {Component,NgZone} from '@angular/core';
import {NavController} from 'ionic-angular';
import {AboutPage} from "../about/about";
import {FaqPage} from "../faq/faq";
import {MyAccountPage} from "../my-account/my-account";
import {SuggestPage} from "../suggest/suggest";
import {ProfilePage} from "../profile/profile";
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import {SocialSharing} from "@ionic-native/social-sharing";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {Config} from "../../services/config";
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    
    //public Message = "הורידו עכשיו את אפליקציית הבלון ,האפליקצייה שמאפשרת לך להרוויח כסף מהבית";
    //public Image = "http://www.tapper.co.il/baloon/logo.png"
    //public PhoneNumber = "+972524641074";
    //public AppUrl = "https://www.hadassahop.org.il/"


    public CustomerId;
    public host;
    CustumerServerData:any = [];
    public imageLogo:any;
    public mainpagevideo:any;
    public website:any;
    public description:any;
    public CustumerName:any;

    public AppTitle:any;
    public AppWebSite:any;
    public AppDesc:any;


    constructor(public navCtrl: NavController,private launchNavigator: LaunchNavigator,private socialSharing: SocialSharing,public sendtoserver:sent_to_server_service,public zone: NgZone , public Settings:Config,private domSanitizer: DomSanitizer) {
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(val => {
            this.zone.run(() => {
                this.CustumerServerData = val;
                console.log("CustumerServerData",this.CustumerServerData)
                if (this.CustumerServerData) {
                    if (this.CustumerServerData.custumer_data) {

                        if (this.CustumerServerData.app_settings.length > 0) {
                            this.AppTitle =  this.CustumerServerData.app_settings[0].title;
                            this.AppWebSite =  this.CustumerServerData.app_settings[0].website;
                            this.AppDesc =  this.CustumerServerData.app_settings[0].description;
                        }

                        if (this.CustumerServerData.custumer_data.length > 0) {
                            this.CustumerName = this.CustumerServerData.custumer_data[0].name;
                            this.imageLogo = this.host + this.CustumerServerData.custumer_data[0].image;
                            this.website = this.CustumerServerData.custumer_data[0].website;
                            this.description = this.CustumerServerData.custumer_data[0].description;
                            this.mainpagevideo = this.CustumerServerData.custumer_data[0].home_video+'?rel=0';
                            this.mainpagevideo = this.domSanitizer.bypassSecurityTrustResourceUrl(this.mainpagevideo);
                        }
                    }
                }
            });
        });
    }
    
    goToAbout() {
        this.navCtrl.push(AboutPage);
    }
    
    goToFAQ() {
        this.navCtrl.push(FaqPage);
    }
    
    goToMyAccount()
    {
        this.navCtrl.push(MyAccountPage);
    }
    
    goToSuggest()
    {
        this.navCtrl.push(SuggestPage);
    }
    
    goToProfile()
    {
        this.navCtrl.push(ProfilePage);
    }
    
    shareViaWhatsApp() {
        //this.socialSharing.shareViaWhatsApp(this.Message , this.Image , this.AppUrl);
        this.socialSharing.shareViaSMS(window.localStorage.name+" " + "שיתף איתך לינק" + " " +  this.description+" "+this.AppWebSite , null  );
        console.log("sssss")
        //this.socialSharing.shareViaFacebook(this.Message, this.Image, this.AppUrl)
    }

}





