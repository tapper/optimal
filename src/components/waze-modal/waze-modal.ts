import { Component ,NgZone} from '@angular/core';
import {ViewController} from "ionic-angular";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {Config} from "../../services/config";
/**
 * Generated class for the WazeModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'waze-modal',
  templateUrl: 'waze-modal.html'
})
export class WazeModalComponent {

  text: string;
    public CustomerId;
    public host;
    CustumerServerData:any = [];
    customers_branches:any = [];
    public imageLogo:any;


  constructor(public viewCtrl: ViewController,public sendtoserver:sent_to_server_service,public zone: NgZone, public Settings:Config) {
      this.CustomerId = this.Settings.CustomerId;
      this.host = this.Settings.host;
      this.sendtoserver._JsonInfo.subscribe(val => {
          this.zone.run(() => {
              this.CustumerServerData = val;
              console.log("CustumerServerData",this.CustumerServerData)
              if (this.CustumerServerData) {
                  if (this.CustumerServerData.customers_branches) {
                      if (this.CustumerServerData.customers_branches.length > 0) {
                          this.customers_branches = this.CustumerServerData.customers_branches;
                      }
                  }
              }
          });
      });
  }

    onSubmit(type)
    {
        this.viewCtrl.dismiss(type);
    }

}
